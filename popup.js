'use strict';
let copy01 = document.getElementById('copy01');
let copy02 = document.getElementById('copy02');
let copy03 = document.getElementById('copy03');

copy01.onclick = function () {
    var code = document.getElementById('note01');
    navigator.clipboard.writeText(code.value);
    console.log("Copied the text: " + code.value);
}

copy02.onclick = function () {
    var code = document.getElementById('note02');
    navigator.clipboard.writeText(code.value);
    console.log("Copied the text: " + code.value);
}

copy03.onclick = function () {
    var code = document.getElementById('note03');
    navigator.clipboard.writeText(code.value);
    console.log("Copied the text: " + code.value);
}


